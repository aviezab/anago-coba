import sys
import anago
import pprint
import os
import tensorflow as tf
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' 

sumber = sys.argv[1]
dirOutput = "/anago/data/ner/model2"
model = anago.Sequence().load(dirOutput)

words = sumber.split()
hasil = model.analyze(words)
pprint.pprint(hasil)
